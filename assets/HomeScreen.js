import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Chip } from 'react-native-paper';

export default function HomeScreen() {
  return (
    <View style={styles.container}>
      <Chip style={styles.chip} icon="newspaper">News 1</Chip>
      <Chip style={styles.chip} icon="newspaper">News 2</Chip>
      <Chip style={styles.chip} icon="newspaper">News 3</Chip>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  chip: {
    margin: 5,
  },
});
