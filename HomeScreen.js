import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

const date = new Date().toLocaleDateString();
const newsItems = [
  {
    id: '1',
    title: 'Lorem ipsum dolor sit amet',
    excerpt: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
    date: date,
    category: 'Політика',
  },
  {
    id: '2',
    title: 'Ut enim ad minim veniam',
    excerpt: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
    date: date,
    category: 'Технології',
  },
  {
    id: '3',
    title: 'Duis aute irure dolor in reprehenderit',
    excerpt: 'Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
    date: date,
    category: 'Розваги',
  },
];

export default function HomeScreen() {
  return (
    <View style={styles.container}>
      {newsItems.map((item) => (
        <TouchableOpacity key={item.id} style={styles.card}>
          <Text style={styles.category}>{item.category}</Text>
          <Text style={styles.title}>{item.title}</Text>
          <Text style={styles.excerpt}>{item.excerpt}</Text>
          <Text style={styles.date}>{item.date}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  card: {
    backgroundColor: '#f1f1f1',
    borderRadius: 10,
    padding: 20,
    marginBottom: 20,
    width: '100%',
  },
  category: {
    fontSize: 14,
    fontWeight: 'bold',
    textTransform: 'uppercase',
    color: 'gray',
    marginBottom: 10,
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  excerpt: {
    fontSize: 16,
    marginBottom: 10,
  },
  date: {
    fontSize: 14,
    color: 'gray',
    alignSelf: 'flex-end',
  },
});
