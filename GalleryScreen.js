import React from 'react';
import { StyleSheet, Text, View } from 'react-native';


export default function GalleryScreen() {
  return (
    <View style={styles.container}>
      <View style={styles.gallery}>
        <View style={styles.tile}>
          <Text style={styles.placeholder}>No Image</Text>
        </View>
        <View style={styles.tile}>
          <Text style={styles.placeholder}>No Image</Text>
        </View>
        <View style={styles.tile}>
          <Text style={styles.placeholder}>No Image</Text>
        </View>
        <View style={styles.tile}>
          <Text style={styles.placeholder}>No Image</Text>
        </View>
        <View style={styles.tile}>
          <Text style={styles.placeholder}>No Image</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  gallery: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    padding: 10,
  },
  tile: {
    width: '50%',
    height: 200,
    padding: 10,
  },
  placeholder: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#ddd',
    alignSelf: 'center',
  },
});
