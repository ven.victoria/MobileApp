import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { AntDesign } from '@expo/vector-icons';
import HomeScreen from './HomeScreen';
import GalleryScreen from './GalleryScreen';
import AccountScreen from './AccountScreen';

const Tab = createBottomTabNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ color, size }) => {
            let iconName;

            if (route.name === 'Home') {
              iconName = 'home';
            } else if (route.name === 'Gallery') {
              iconName = 'picture';
            } else if (route.name === 'Account') {
              iconName = 'user';
            }

            return <AntDesign name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: 'tomato',
          inactiveTintColor: 'gray',
        }}
      >
        <Tab.Screen name="Home" component={HomeScreen} options={{title: 'Головна'}} />
        <Tab.Screen name="Gallery" component={GalleryScreen} options={{title: 'Галерея'}}/>
        <Tab.Screen name="Account" component={AccountScreen} options={{title: 'Акаунт'}}/>
      </Tab.Navigator>
    </NavigationContainer>
  );
}
